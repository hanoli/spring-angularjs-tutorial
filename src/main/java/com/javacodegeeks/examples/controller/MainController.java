package com.javacodegeeks.examples.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.crypto.Data;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class MainController {
	
	private static final int BUFFER_SIZE = 4096;
	private String filePath = "C:/DemoUpload/";
	
	
    @RequestMapping("/")
    public String homepage(){
        return "homepage";
    }
    
        
    @RequestMapping(value = "/fileUpload", method = RequestMethod.POST)
    public @ResponseBody String continueFileUpload(HttpServletRequest request, HttpServletResponse response){
    	
    	System.out.println("Llegue al controller");
        
    	MultipartHttpServletRequest mRequest;
    	
    	try {
    	   mRequest = (MultipartHttpServletRequest) request;
    	   mRequest.getParameterMap();
    	   
    	   File dir = new File(filePath);
			if (!dir.exists())
				dir.mkdirs();
			
    	   Iterator itr = mRequest.getFileNames();
    	   while (itr.hasNext()) {
    	        MultipartFile mFile = mRequest.getFile((String) itr.next());
    	        String fileName = mFile.getOriginalFilename();
    	
    	        java.nio.file.Path path = Paths.get( filePath + fileName);
    	        Files.deleteIfExists(path);
    	        InputStream in = mFile.getInputStream();
    	        Files.copy(in, path);
    		 }
    	   } catch (Exception e) {
    	        e.printStackTrace();
    	   }
    	return "El archivo se subio con exito!";
    	}
    
    
}
